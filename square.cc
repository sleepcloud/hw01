#include<iostream>
using namespace std;
int clean_square(int**p,int len){

	int i;

	for(i=0;i<len;i++){

		p[i]=new int[len];

				}

	return 0;

}
int make_square(int**p,int len){
	int row=0;	
	int col=len/2;
	int i;
	int n=1;
	p[row][col]=n;
	for(i=1;i<len*len;i++){
	if(row-1<0 && col+1 > len-1){
		p[++row][col]=++n;
				}
	else if(row-1<0 && col+1 <=len-1){
		row=len-1;		
		p[row][++col]=++n;
				}
	else if(row-1>=0 && col+1> len-1){
		row--;
		col=0;
		p[row][col]=++n;
				}
	else if(p[row-1][col+1]!=0){
		p[++row][col]=++n;	
	}
	else p[--row][++col]=++n;
	}
}

int print_square(int**p,int len){
	int i;
	int j;
	for(i=0;i<len;i++){
		for(j=0;j<len;j++){
			cout<<p[i][j]<<" ";
				}
		cout<<endl;
	}
}
int main(){
	int square_len;
	cin>>square_len;
	if((square_len)%2==1 && square_len>0){
	int**p=new int*[square_len];
	clean_square(p,square_len);	
	make_square(p,square_len);
	print_square(p,square_len);	
	delete p;			}
	
	
	return 0;
}